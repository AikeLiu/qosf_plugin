import unittest

from braket.aws import AwsDevice, AwsDeviceType
from braket.devices import LocalSimulator
from qiskit.exceptions import QiskitError
from qiskit_braket_provider.braket_provider import Braket_Provider


class TestProvider(unittest.TestCase):
    def test_provider_getbackend(self):
        """Verifies that provider.get_backend works."""
        pro = Braket_Provider()
        for backend in pro.backends():
            print(backend.name())
            self.assertTrue(
                type(backend)
                is type(
                    pro.get_backend(
                        local=backend.configuration().local, Name=backend.name()
                    )
                )
            )

    def test_provider_localsimulator(self):
        pro = Braket_Provider()
        self.assertTrue(
            isinstance(
                pro.get_backend(local=True, Name="DefaultSimulator")._device,
                LocalSimulator,
            )
        )

    def test_provider_AWSsimulator(self):
        pro = Braket_Provider()
        dev = AwsDevice.get_devices(names="SV1")[0]
        self.assertTrue(dev == pro.get_backend(local=False, Name="SV1")._device)

    @unittest.expectedFailure
    def test_invalid_getbackend(self):
        pro = Braket_Provider()
        self.assertRaises(TypeError, pro.get_backend())

    @unittest.expectedFailure
    def test_invalid_get_local_backend(self):
        self.assertRaises(QiskitError, pro.get_backend(local=True))

    @unittest.expectedFailure
    def test_invalid_get_AWS_backend(self):
        self.assertRaises(QiskitError, pro.get_backend(local=False))

    @unittest.expectedFailure
    def test_unavailable_local_devices(self):
        self.assertRaises(QiskitError, pro.get_backend(local=True, Name="future"))

    @unittest.expectedFailure
    def test_unavailable_AWS_devices(self):
        self.assertRaises(QiskitError, pro.get_backend(local=False, Name="TN1"))
