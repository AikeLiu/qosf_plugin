import warnings

from qiskit.providers import ProviderV1

from .braket_backend import AWS_Backend, BraketLocal


class Braket_Provider(ProviderV1):
    def __init__(self):
        super().__init__()
        self.access_token = None
        self.name = "aws_provider"
        # Populate the list of backends, in the form of tuples (LOCAL: bool, Name: str)
        # The latest information of the supported devices can be found here:
        # https://docs.aws.amazon.com/braket/latest/developerguide/braket-devices.html
        self.list_of_backends = [
            (True, "DefaultSimulator"),
            (True, "DM"),
            (False, "SV1"),
            (False, "TN1"),
            (False, "DM1"),
        ]

    def __str__(self):
        return "<AWSProvider(name={})>".format(self.name)

    def __repr__(self):
        return self.__str__()

    def get_backend(self, local, Name=None, Arn=None, filters=None, **kwargs):
        """
        Argus:
            local: bool. To indicates whether the costumer wants a local or a remote aws device
            Name: str. The name of the device
            Arn: str. The arn of the device
        Return:
            The backend requested, accessed through Qiskit framework
        """
        if not local:
            return AWS_Backend(name=Name, arn=Arn, provider=self)
        return BraketLocal(name=Name, provider=self)

    def backends(self, filters=None, **kwargs):
        """
        Return:
            A list of all backends accessible from the provider
        """
        L = []
        for (location, backend_name) in self.list_of_backends:
            try:
                L.append(self.get_backend(local=location, Name=backend_name))
            except:
                if location:
                    warnings.warn(
                        "%s is not supported on your local computer, please check the version of Braket installed."
                        % backend_name,
                        UserWarning,
                    )
                else:
                    warnings.warn(
                        "%s is not supported in your current region." % backend_name,
                        UserWarning,
                    )
        return L
