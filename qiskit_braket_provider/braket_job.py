from braket.aws import AwsDevice, AwsQuantumTask
from qiskit.providers import JobV1 #JobError, JobTimeoutError, JobV1
from qiskit.providers.jobstatus import JobStatus
from qiskit.result import Result

from .qiskit_to_braket import map_qiskit_circuit_to_braket_circuit


class Braket_Job(JobV1):
    def __init__(
        self,
        backend,
        s3_folder=None,
        shots=100,
        qcircuit=None,
    ):
        """
        Initialize a job instance.
        Args:
            backend (qiskit BackendV1): Backend that job to be executed on, the choice can be BraketLocal or AWS_Backend.
            s3_folder: AWS cloud folder to store the computation results, required when using a remote AWS device only.
            shots: number of experiments
            access_token: not required
            job_id: not required
            qcircuit: qiskit circuits put in by customers.

        Parameters:
            _backend, qcircuit, s3_folder, shots: specified by input
            braket_circuit: translated from qcircuit
            _braket_task: a task submitted to either a local device or aws device from which the results are to be retrived
            _job_id: The unique job ID/arn associated with the _braket_task.
        """
        self.shots = shots
        self._backend = backend
        self.qcircuit = qcircuit
        self._s3_folder = s3_folder

        self.braket_circuit = map_qiskit_circuit_to_braket_circuit(
            qcircuit, self._backend._configuration.basis_gates
        )

        # special case: when the costumer request a device which support measuring state vector
        for r_type in self._backend._configuration.result_types:
            if r_type.name == "State vector":
                self.braket_circuit.state_vector()

        # submit jobs
        if self._backend._configuration.local:
            self._braket_task = self._backend._device.run(
                self.braket_circuit, shots=self.shots
            )
            self._job_id = None
        else:
            self._braket_task = None
            self._job_id = self._backend._device.run(
                self.braket_circuit, self._s3_folder, shots=self.shots
            )._arn
        super().__init__(backend, self._job_id)

    def _braket_to_qiskit_data(self, braket_result):
        """
        Args:
            Results returned from a braket task: task.result()
        Return:
            A dictionary of ExperimentResultData
        """
        if braket_result.result_types == [] and braket_result.task_metadata.shots > 0:
            out_dict = {
                "counts": {
                    state[::-1]: count
                    for state, count in braket_result.measurement_counts.items()
                }
            }
        else:
            for result_Type in braket_result.result_types:
                type_name = result_types.type.type
                if type_name == "state_vector":
                    state_vec = np.zeros(self.braket_circuit.qubit_count, dtype=complex)
                    for key, value in result_type.value.items():
                        state_vec[int(key, 2)] = value
                    out_dict[type_name] = {"statevector": state_vec}
                else:
                    # in braket: Amplitude, Expectation, Probability, Sample, Variance
                    # in qiskit: snapshots (dict), memory (list), unitary (list or numpy.array), kwargs (any): additional data key-value pairs.
                    out_dict[type_name] = result_types.value  # TODO
        return out_dict

    def _braket_to_qiskit_result(self, braket_result):
        """
        Args:
            Results returned from a braket task: task.result()
        Return:
            ExperimentResult in Qiskit framework
        """
        results = [
            {
                "success": True,
                "shots": braket_result.task_metadata.shots,
                "data": self._braket_to_qiskit_data(braket_result)
                #'header': None #{'memory_slots': self.qobj.config.memory_slots,
                # 'name': self.qobj.experiments[0].header.name}
            }
        ]
        return Result.from_dict(
            {
                "results": results,
                "backend_name": self._backend._configuration.backend_name,
                "backend_version": self._backend._configuration.backend_version,
                "qobj_id": ", ".join(x[0].name for x in self.qcircuit),
                "success": True,
                "job_id": self._job_id,
            }
        )

    def result(self, timeout=None, wait=None):
        """
        Args: timeout and wait specified by costumers. If not specified, the default parameters in AwsQuantumTask are used
        Return: Qiskit ExperimentResult
        """
        if not self._backend._configuration.local:
            if timeout == None:
                timeout = AwsQuantumTask.DEFAULT_RESULTS_POLL_TIMEOUT
            if wait == None:
                wait = AwsQuantumTask.DEFAULT_RESULTS_POLL_INTERVAL
            self._braket_task = AwsQuantumTask(
                arn=self._job_id,
                poll_timeout_seconds=timeout,
                poll_interval_seconds=wait,
            )
        qresult = self._braket_to_qiskit_result(self._braket_task.result())
        return qresult

    def status(self):
        """
        checking status
        """
        braket_status = self._braket_task.state()
        if braket_status == "COMPLETED":
            status = JobStatus.DONE
        elif braket_status == "CANCELLED":
            status = JobStatus.CANCELLED
        elif braket_status == "FAILED":
            status = JobStatus.ERROR
        else:
            status = JobStatus.RUNNING
        return status

    def cancel(self):
        # TODO ?
        raise NotImplementedError

    def submit(self, timeout=432000, wait=5):
        # TODO ?
        raise NotImplementedError
